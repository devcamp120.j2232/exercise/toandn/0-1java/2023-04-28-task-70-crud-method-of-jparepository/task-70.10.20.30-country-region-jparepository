package com.devcamp.pizza365.repository;

import java.util.*;
import com.devcamp.pizza365.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);

	CCountry findByCountryName(String countryName);

	List<CCountry> findByRegions_RegionCode(String regionCode);

	List<CCountry> findByRegions_RegionName(String regionName);

	CCountry findByCountryCodeContaining(String countryCode);

}
